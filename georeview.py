import pandas as pd
from numpy import nan
from sys import argv
from math import sqrt

# pandas SettingWithCopyWarning is a false positive for this script and is disabled
pd.options.mode.chained_assignment = None

# input file name is written as argument
filename = argv[1]

print("Merging files...\n")

# import original file plus both Google and Bing outputs
try:
	add_in = pd.read_csv(filename + ".csv", index_col = False)
except:
	add_in = pd.read_csv(filename + ".csv", encoding = "ISO-8859-1", index_col = False)

try:
	g_out = pd.read_csv(filename + "_google output.csv", index_col = False)
except UnicodeDecodeError:
	g_out = pd.read_csv(filename + "_google output.csv", encoding = "ISO-8859-1", index_col = False)

try:
	b_out = pd.read_csv(filename + "_bing output.csv", index_col = False)
except UnicodeDecodeError:
	b_out = pd.read_csv(filename + "_bing output.csv", encoding = "ISO-8859-1", index_col = False)

# REMOVE ERROR CASE TEMPORARILY, DELETE SCRIPT WHEN UPDATED FILES ARE AVAILABLE
# add_in.drop(283, inplace = True)
# g_out.drop(283, inplace = True)
# b_out.drop(283, inplace = True)

# Project Accuracy Codes:
# 1a = House address
# 1b = Intersection
# 1c = MNR Station Location
# 2a = Street centroid + ZIP code
# 2b = Street centroid (no ZIP code)
# 3a = ZIP code centroid
# 3b = Point of Interest / Neighborhood
# 4 = Municipality

# RECODE GOOGLE ADDRESS TYPE TO ACCURACY LEVEL

# crosswalk for Address Type -> Accuracy
g_accu = {
	"locality" : "4",
	"sublocality" : "4",
	"neighborhood" : "3b",
	"postal_code" : "3a",
	"route" : "2",
	"intersection" : "1b",
	"establishment" : "1a",
	"street_address" : "1a",
	"premise" : "1a",
	"No Results Found" : "0"
}

# create new column to track Google Accuracy
g_out["Google Accuracy"] = nan

# search for Accuracy in Address Type list Google Address Type, assign Accuracy accordingly
for i in range(g_out.shape[0]):
	c = 0
	while c < len(g_accu):
		if list(g_accu)[c] not in g_out["Google Address Type"].iloc[i]:
			c += 1
		else:
			g_out["Google Accuracy"].iloc[i] = g_accu[list(g_accu)[c]]

			# specify 2a or 2b for routes based on whether there is an output ZIP code or not
			if g_out["Google Accuracy"].iloc[i] == "2" and pd.notnull(g_out["Google ZIP"].iloc[i]):
				g_out["Google Accuracy"].iloc[i] = "2a"
			elif g_out["Google Accuracy"].iloc[i] == "2" and pd.isnull(g_out["Google ZIP"].iloc[i]):
				g_out["Google Accuracy"].iloc[i] = "2b"

			c += 1

# RECODE BING ADDRESS TYPE TO ACCURACY LEVEL

# crosswalk for Address Type -> Accuracy
b_accu = {
	"PopulatedPlace" : "4",
	"Neighborhood" : "3b",
	"Park" : "3b",
	"Postcode1" : "3a",
	"RoadBlock" : "2",
	"RoadIntersection" : "1b",
	"Address" : "1a",
	"Airport" : "1a",
	"HigherEducationFacility" : "1a",
	"LandmarkBuilding" : "1a",
	"HistoricalSite" : "1a",
	"PointOfInterest" : "1a",
	"Stadium" : "1a",
	"TouristStructure" : "1a",
	"No Results Found" : "0"
}

# create new column to track Bing Accuracy
b_out["Bing Accuracy"] = nan

# search for Accuracy in Address Type list Bing Address Type, assign Accuracy accordingly
for i in range(b_out.shape[0]):
	c = 0
	while c < len(b_accu):
		if list(b_accu)[c] not in b_out["Bing Address Type"].iloc[i]:
			c += 1
		else:
			b_out["Bing Accuracy"].iloc[i] = b_accu[list(b_accu)[c]]

			# specify 2a or 2b for routes based on whether there is an output ZIP code or not
			if b_out["Bing Accuracy"].iloc[i] == "2" and pd.notnull(b_out["Bing ZIP"].iloc[i]):
				b_out["Bing Accuracy"].iloc[i] = "2a"
			elif b_out["Bing Accuracy"].iloc[i] == "2" and pd.isnull(b_out["Bing ZIP"].iloc[i]):
				b_out["Bing Accuracy"].iloc[i] = "2b"

			c += 1

# merge input, Google output, and Bing output into one df
merge_out = pd.merge(add_in, 
	g_out[["RecordNo", "Q", "Google Address", "Google City", "Google State", "Google ZIP", "Google Country", "Google Formatted Address",
	"Google Latitude", "Google Longitude", "Google Address Type", "Google Accuracy"]],
	how = "left", on = ["RecordNo", "Q"])

merge_out = pd.merge(merge_out,
	b_out[["RecordNo", "Q", "Bing Address", "Bing City", "Bing State", "Bing ZIP", "Bing Country", "Bing Formatted Address", "Bing Latitude",
	"Bing Longitude", "Bing Address Type", "Bing Confidence", "Bing Match Code", "Bing Accuracy"]],
	how = "left", on = ["RecordNo", "Q"])

# create new column to track parsing
merge_out["Address Flag"] = nan

# define different parse flags
parse_type = {
	1 : "Possible House Address",
	2 : "Possible Intersection",
	3 : "Misc Address or Route Only",
	4 : "ZIP Code Only",
	5 : "City Only",
	6 : "Other Records (Should be blank)",
	7 : "Global Review Records"
	}

#the following loops use iloc instead of loc because indexes become nonconsecutive due to slicing

print("Applying Global Review flags...")

# parse global review cleans
merge_out["Global Flag"] = nan

# tristate area for flagging
tristate = ["NY", "NJ", "CT"]

for i in range(merge_out.shape[0]):

	# review if state is blank
	if (pd.isnull(merge_out["State"].iloc[i]) and
		pd.isnull(merge_out["Google State"].iloc[i]) and
		pd.isnull(merge_out["Bing State"].iloc[i])):
		merge_out["Global Flag"].iloc[i] = 1

	# review if neither output is within the tristate area
	elif merge_out["Google State"].iloc[i] not in tristate and merge_out["Bing State"].iloc[i] not in tristate:
		merge_out["Global Flag"].iloc[i] = 1

	# review if input city was New York and neither output city is New York, but only if Accuracy = 1b
	elif (str(merge_out["City"].iloc[i]).upper() == "NEW YORK" and
		merge_out["Google Accuracy"].iloc[i] == "1b" and
		str(merge_out["Google City"].iloc[i]).upper() != "NEW YORK" and
		merge_out["Bing Accuracy"].iloc[i] == "1b" and
		str(merge_out["Bing City"].iloc[i]).upper() != "NEW YORK"):
		merge_out["Global Flag"].iloc[i] = 1

global_review = merge_out[merge_out["Global Flag"] == 1]
merge_out = merge_out[pd.isnull(merge_out["Global Flag"])]

# parse potential intersections
for i in range(merge_out.shape[0]):

	print("Parsing intersections... " + str(i + 1) + "/" + str(merge_out.shape[0]) +
		" records remaining. (" + "{0:.0%}".format((i + 1)/merge_out.shape[0]) + ")", end = "\r")

	add_parse = str(merge_out["Address"].iloc[i]).split(" ")
	try:
		if len(add_parse) > 1 and "AND" in add_parse:
			merge_out["Address Flag"].iloc[i] = 2
	except ValueError:
		merge_out["Address Flag"].iloc[i] = nan


print("\n")

intersection_out = merge_out[merge_out["Address Flag"] == 2]
merge_out = merge_out[pd.isnull(merge_out["Address Flag"])]

print(str(intersection_out.shape[0]) + " intersection records identified.")

# parse potential house addresses
for i in range(merge_out.shape[0]):

	print("Parsing house addresses... " + str(i + 1) + "/" + str(merge_out.shape[0]) +
		" records remaining. (" + "{0:.0%}".format((i + 1)/merge_out.shape[0]) + ")", end = "\r")

	add_parse = str(merge_out["Address"].iloc[i]).split(" ")
	try:
		if len(add_parse) > 1:
			int(add_parse[0])
			merge_out["Address Flag"].iloc[i] = 1
	except ValueError:
		merge_out["Address Flag"].iloc[i] = nan

print("\n")

house_out = merge_out[merge_out["Address Flag"] == 1]
merge_out = merge_out[pd.isnull(merge_out["Address Flag"])]

print(str(house_out.shape[0]) + " house address records identified.")

# parse all other records where Address is not blank
for i in range(merge_out.shape[0]):

	print("Parsing all other addresses... " + str(i + 1) + "/" + str(merge_out.shape[0]) +
		" records remaining. (" + "{0:.0%}".format((i + 1)/merge_out.shape[0]) + ")", end = "\r")

	if pd.notnull(merge_out["Address"].iloc[i]) and merge_out["Address"].iloc[i] != " ":
		merge_out["Address Flag"].iloc[i] = 3
	else:
		merge_out["Address Flag"].iloc[i] = nan

print("\n")

street_out = merge_out[merge_out["Address Flag"] == 3]
merge_out = merge_out[pd.isnull(merge_out["Address Flag"])]

print(str(street_out.shape[0]) + " other street records identified.")

# parse all remaining records with ZIP code
for i in range(merge_out.shape[0]):

	print("Parsing ZIP codes... " + str(i + 1) + "/" + str(merge_out.shape[0]) +
		" records remaining. (" + "{0:.0%}".format((i + 1)/merge_out.shape[0]) + ")", end = "\r")

	if pd.notnull(merge_out["ZIP"].iloc[i]) and merge_out["ZIP"].iloc[i] != " ":
		merge_out["Address Flag"].iloc[i] = 4
	else:
		merge_out["Address Flag"].iloc[i] = nan

print("\n")

zip_out = merge_out[merge_out["Address Flag"] == 4]
merge_out = merge_out[pd.isnull(merge_out["Address Flag"])]

print(str(zip_out.shape[0]) + " ZIP code records identified.")

# parse all remaining records with City only
for i in range(merge_out.shape[0]):

	print("Parsing cities... " + str(i + 1) + "/" + str(merge_out.shape[0]) +
		" records remaining. (" + "{0:.0%}".format((i + 1)/merge_out.shape[0]) + ")", end = "\r")

	if pd.notnull(merge_out["City"].iloc[i]) and merge_out["City"].iloc[i] != " ":
		merge_out["Address Flag"].iloc[i] = 5
	else:
		merge_out["Address Flag"].iloc[i] = nan

print("\n")

city_out = merge_out[merge_out["Address Flag"] == 5]
# State field is not parsed for, last merge_out dataframe should be blank (all records should fall into one of the 5 pools above)
merge_out = merge_out[pd.isnull(merge_out["Address Flag"])]

print(str(city_out.shape[0]) + " city records identified.")

# # REMOVE AT END, USED FOR TESTING ONLY, FINAL OUTPUT WILL BE TO CSV
# writer = pd.ExcelWriter(filename + "_out.xlsx")
# house_out.to_excel(writer, sheet_name = parse_type[1])
# intersection_out.to_excel(writer, sheet_name = parse_type[2])
# street_out.to_excel(writer, sheet_name = parse_type[3])
# zip_out.to_excel(writer, sheet_name = parse_type[4])
# city_out.to_excel(writer, sheet_name = parse_type[5])
# merge_out.to_excel(writer, sheet_name = parse_type[6])

print("Parsing complete. " + str(merge_out.shape[0]) + " records left unparsed.")

# Rules for accepting house address

print("\n")
print("Applying parsing rules for house addresses...")

# ---Step 0: Create new columns for keeping track of decisions---
house_out["Acceptable Distance"] = nan # for comparing distances between points
house_out["Accept"] = nan # 1 = accept Google output 2 = accept Bing output 0 = review nan = review
							# (if both outputs are acceptable, accept Google output)

# for checking parsing rules, will be deleted from final version
house_out["Parse"] = nan

for i in range(house_out.shape[0]):

	g_is_house = 0
	g_house = str(house_out["Google Address"].iloc[i]).split(" ")

	try:
		if len(g_house) > 1:
			int(g_house[0])
			g_is_house = 1
	except ValueError:
		g_is_house = 0

	# ---Step 1: Calculate distance between points---
	dist = (sqrt((house_out["Google Latitude"].iloc[i] - house_out["Bing Latitude"].iloc[i])**2 +
			(house_out["Google Longitude"].iloc[i] - house_out["Bing Longitude"].iloc[i])**2))
	# flag if distance is <= 200m
	if dist <= 0.002:
		house_out["Acceptable Distance"].iloc[i] = 1
	else:
		house_out["Acceptable Distance"].iloc[i] = 0		

	# ---Step 2: Only consider addresses where both Google Accuracy and Bing Accuracy = 1a---
	if house_out["Google Accuracy"].iloc[i] == "1a" and house_out["Bing Accuracy"].iloc[i] == "1a":
		
		# ---Step 3: If Google Address/City and Bing Address/City match, accept Bing output---
		if (house_out["Google Address"].iloc[i] == house_out["Bing Address"].iloc[i] and
			house_out["Google City"].iloc[i] == house_out["Bing City"].iloc[i]):
			house_out["Accept"].iloc[i] = 2
			house_out["Parse"].iloc[i] = "Step 3"

		# ---Step 4: If the distance between points is <= 200m, accept Bing---
		elif house_out["Acceptable Distance"].iloc[i] == 1:
			house_out["Accept"].iloc[i] = 2
			house_out["Parse"].iloc[i] = "Step 4"

		# # ---Step 5: If the distance between points is > 200m, but the first 3 digits of ZIP codes match, accept Bing---
		# elif (house_out["Acceptable Distance"].iloc[i] == 0 and
		#  	pd.notnull(house_out["Google ZIP"].iloc[i]) and pd.notnull(house_out["Bing ZIP"].iloc[i]) and
		# 	str(int(house_out["Google ZIP"].iloc[i])).zfill(5)[:3] == str(int(house_out["Bing ZIP"].iloc[i])).zfill(5)[:3]):
		# 	house_out["Accept"].iloc[i] = 2
		# 	house_out["Parse"].iloc[i] = "Step 5"

		# ---Step 5: If the distance between points is > 200m, but an output city matches the input city, accept that output---
		elif (type(house_out["Google City"].iloc[i]) is str and type(house_out["City"].iloc[i]) is str and 
			house_out["Google City"].iloc[i].upper() == house_out["City"].iloc[i].upper() and g_is_house == 1):
			house_out["Accept"].iloc[i] = 1
			house_out["Parse"].iloc[i] = "Step 5G"

		elif (type(house_out["Bing City"].iloc[i]) is str and type(house_out["City"].iloc[i]) is str and
			house_out["Bing City"].iloc[i].upper() == house_out["City"].iloc[i].upper()):
			house_out["Accept"].iloc[i] = 2
			house_out["Parse"].iloc[i] = "Step 5B"

		# ---Step 6: Review all others---		
		else:
			house_out["Accept"].iloc[i] = 0	

# Rules for accepting intersections

print("Applying parsing rules for intersections...")

# ---Step 0: Create new column for keeping track of decisions---
intersection_out["Accept"] = nan # 1 = accept Google output 2 = accept Bing output 0 = review nan = review 
								# (if both outputs are acceptable, accept Google output)

# for checking parsing rules, will be deleted from final version
intersection_out["Parse"] = nan

for i in range(intersection_out.shape[0]):

	# ---Step 1: If Google Accuracy = 1b and Google City matches input City, accept Google---
	if (intersection_out["Google Accuracy"].iloc[i] == "1b" and
		type(intersection_out["Google City"].iloc[i]) is str and type(intersection_out["City"].iloc[i]) is str and
		intersection_out["Google City"].iloc[i].upper() == intersection_out["City"].iloc[i].upper() and
		"&" in intersection_out["Google Address"].iloc[i]):
		intersection_out["Accept"].iloc[i] = 1
		intersection_out["Parse"].iloc[i] = "Step 1"

	# ---Step 2: If Bing Accuracy = 1b and Bing City matches input City, accept Bing (Bing Address must also include "&" character)---
	elif (intersection_out["Bing Accuracy"].iloc[i] == "1b" and
		type(intersection_out["Bing City"].iloc[i]) is str and type(intersection_out["City"].iloc[i]) is str and
		intersection_out["Bing City"].iloc[i].upper() == intersection_out["City"].iloc[i].upper() and
		"&" in intersection_out["Bing Address"].iloc[i]): 
		intersection_out["Accept"].iloc[i] = 2
		intersection_out["Parse"].iloc[i] = "Step 2"

	# ---Step 3: If Google and Bing Accuracy = 1b and Bing City matches Google City, accept Google---
	elif (intersection_out["Google Accuracy"].iloc[i] == "1b" and intersection_out["Bing Accuracy"].iloc[i] == "1b" and
		type(intersection_out["Google City"].iloc[i]) is str and type(intersection_out["Bing City"].iloc[i]) is str and
		intersection_out["Google City"].iloc[i].upper() == intersection_out["Bing City"].iloc[i].upper() and
		"&" in intersection_out["Google Address"].iloc[i]):
		intersection_out["Accept"].iloc[i] = 1
		intersection_out["Parse"].iloc[i] = "Step 3"

	# # ---Step 4: If Bing Accuracy = 1b and Bing ZIP = Input ZIP (and Bing Address has an "&"), accept Bing---
	elif (intersection_out["Bing Accuracy"].iloc[i] == "1b" and
		pd.notnull(intersection_out["Bing ZIP"].iloc[i]) and pd.notnull(intersection_out["ZIP"].iloc[i]) and
		intersection_out["Bing ZIP"].iloc[i] != " " and intersection_out["ZIP"].iloc[i] != " " and
		str(int(intersection_out["Bing ZIP"].iloc[i])).zfill(5) == str(int(intersection_out["ZIP"].iloc[i])).zfill(5) and
		"&" in intersection_out["Bing Address"].iloc[i]): 
		intersection_out["Accept"].iloc[i] = 2
		intersection_out["Parse"].iloc[i] = "Step 4"

	# # ---Step 5: If Google Accuracy = 1b and Google ZIP = Input ZIP, accept Google---
	elif (intersection_out["Google Accuracy"].iloc[i] == "1b" and
		pd.notnull(intersection_out["Google ZIP"].iloc[i]) and pd.notnull(intersection_out["ZIP"].iloc[i]) and
		intersection_out["Google ZIP"].iloc[i] != " " and intersection_out["ZIP"].iloc[i] != " " and
		str(int(intersection_out["Google ZIP"].iloc[i])).zfill(5) == str(int(intersection_out["ZIP"].iloc[i])).zfill(5) and
		"&" in intersection_out["Google Address"].iloc[i]): 
			intersection_out["Accept"].iloc[i] = 1
			intersection_out["Parse"].iloc[i] = "Step 5"

	# ---Step 6: Review all others---		
	else:
		intersection_out["Accept"].iloc[i] = 0		

# Rules for accepting Misc. Address / Route Only

print("Applying parsing rules for all other addresses...")

# ---Step 0: Create new column for keeping track of decisions---
street_out["Acceptable Distance"] = nan # for comparing distances between points
street_out["Accept"] = nan # 1 = accept Google output 2 = accept Bing output 0 = review nan = review
							# (if both outputs are acceptable, accept Google output)

# for checking parsing rules, will be deleted from final version
street_out["Parse"] = nan

for i in range(street_out.shape[0]):

	# ---Step 1: Calculate distance between points---
	dist = (sqrt((street_out["Google Latitude"].iloc[i] - street_out["Bing Latitude"].iloc[i])**2 +
			(street_out["Google Longitude"].iloc[i] - street_out["Bing Longitude"].iloc[i])**2))
	# flag if distance is <= 2000m
	if dist <= 0.02:
		street_out["Acceptable Distance"].iloc[i] = 1
	else:
		street_out["Acceptable Distance"].iloc[i] = 0

	# ---Step 2: If Google and Bing Accuracy = 2a/2b and distance between points is <= 2000m, accept Google---
	if (str(street_out["Google Accuracy"].iloc[i])[:1] == "2" and str(street_out["Bing Accuracy"].iloc[i])[:1] == "2" and
		street_out["Acceptable Distance"].iloc[i] == 1):
		street_out["Accept"].iloc[i] = 1
		street_out["Parse"].iloc[i] = "Step 2"

	# ---Step 3: If Bing Accuracy = 2a and Bing ZIP = input ZIP and 2/3 of  Input Address is in Bing Address, accept Bing---
	elif (street_out["Bing Accuracy"].iloc[i] == "2a" and
		pd.notnull(street_out["Bing ZIP"].iloc[i]) and pd.notnull(street_out["ZIP"].iloc[i]) and
		street_out["Bing ZIP"].iloc[i] != " " and street_out["ZIP"].iloc[i] != " " and
		str(int(street_out["Bing ZIP"].iloc[i])).zfill(5) == str(int(street_out["ZIP"].iloc[i])).zfill(5)):

		parse_input = street_out["Address"].iloc[i].split(" ")
		parse_bing = street_out["Bing Address"].iloc[i].split(" ")

		counter = 0

		for item in parse_bing:
			if item in parse_input:
				counter += 1

		if counter / len(parse_input) > 0.66:
			street_out["Accept"].iloc[i] = 2
			street_out["Parse"].iloc[i] = "Step 3"

	# ---Step 4: If Google Accuracy = 2a and Google ZIP = input ZIP, accept Google---
	elif (street_out["Google Accuracy"].iloc[i] == "2a" and
		pd.notnull(street_out["Google ZIP"].iloc[i]) and pd.notnull(street_out["ZIP"].iloc[i]) and
		street_out["Google ZIP"].iloc[i] != " " and street_out["ZIP"].iloc[i] != " " and
		str(int(street_out["Google ZIP"].iloc[i])).zfill(5) == str(int(street_out["ZIP"].iloc[i])).zfill(5)):
		street_out["Accept"].iloc[i] = 1
		street_out["Parse"].iloc[i] = "Step 4"

	# ---Step 5: If Google Accuracy and Bing Accuracy = 1a or 1b and the distance between coordinates <200m, accept Google---
	elif (((street_out["Google Accuracy"].iloc[i] == "1a" and street_out["Bing Accuracy"].iloc[i] == "1a") or
		(street_out["Google Accuracy"].iloc[i] == "1b" and street_out["Bing Accuracy"].iloc[i] == "1b")) and dist <= 0.002):
		street_out["Accept"].iloc[i] = 1
		street_out["Parse"].iloc[i] = "Step 5"

	# ---Step 6: If Google Accuracy and Bing Accuracy = 3b and Google Address = Input Address, accept Google---		
	elif (street_out["Google Accuracy"].iloc[i] == "3b" and street_out["Bing Accuracy"].iloc[i] == "3b" and
		street_out["Google Address"].iloc[i] == street_out["Address"].iloc[i]):
		street_out["Accept"].iloc[i] = 1
		street_out["Parse"].iloc[i] = "Step 6"

	# ---Step 7: Review all others---		
	else:
		street_out["Accept"].iloc[i] = 0		

# Rules for accepting ZIP codes

print("Applying parsing rules for ZIP codes...")

# ---Step 0: Create new column for keeping track of decisions---
zip_out["Accept"] = nan # 1 = accept Google output 2 = accept Bing output 0 = review nan = review
						# (if both outputs are acceptable, accept Google output)

# for checking parsing rules, will be deleted from final version
zip_out["Parse"] = nan

for i in range(zip_out.shape[0]):
	# ---Step 1: If Bing Accuracy = 3a Bing ZIP matches Input ZIP, accept Bing---
	# (ZIP is formatted to ensure both formats are the same)
	if zip_out["Bing Accuracy"].iloc[i] == "3a" and str(int(zip_out["Bing ZIP"].iloc[i])).zfill(5) == str(int(zip_out["ZIP"].iloc[i])).zfill(5):
		zip_out["Accept"].iloc[i] = 2
		zip_out["Parse"].iloc[i] = "Step 1"

	# ---Step 2: If Google Accuracy = 3a Google ZIP matches Input ZIP, accept Google---
	# (ZIP is formatted to ensure both formats are the same)
	elif zip_out["Google Accuracy"].iloc[i] == "3a" and str(int(zip_out["Google ZIP"].iloc[i])).zfill(5) == str(int(zip_out["ZIP"].iloc[i])).zfill(5):
		zip_out["Accept"].iloc[i] = 1
		zip_out["Parse"].iloc[i] = "Step 2"

	# ---Step 3: Review all others---
	else:
		zip_out["Accept"].iloc[i] = 0

# Rules for accepting cities

print("Applying parsing rules for cities..")

# ---Step 0: Create new column for keeping track of decisions---
city_out["Accept"] = nan # 1 = accept Google output 2 = accept Bing output 0 = review nan = review
							# (if both outputs are acceptable, accept Google output)

# for checking parsing rules, will be deleted from final version
city_out["Parse"] = nan

for i in range(city_out.shape[0]):
	# ---Step 1: If Google Accuracy = 4 or 3b and Google City matches Input City, accept Google---
	if ((city_out["Google Accuracy"].iloc[i] == "4" or city_out["Google Accuracy"].iloc[i] == "3b") and
	type(city_out["Google City"].iloc[i]) is str and type(city_out["City"].iloc[i]) is str and
	city_out["Google City"].iloc[i].upper() == city_out["City"].iloc[i].upper()):
		city_out["Accept"].iloc[i] = 1
		city_out["Parse"].iloc[i] = "Step 1"

	# ---Step 2: Else if Bing Accuracy = 4 and Bing City matches Input City, accept Bing---
	elif (city_out["Bing Accuracy"].iloc[i] == "4" and
		type(city_out["Bing City"].iloc[i]) is str and type(city_out["City"].iloc[i]) is str and
		city_out["Bing City"].iloc[i].upper() == city_out["City"].iloc[i].upper()):
		city_out["Accept"].iloc[i] = 2
		city_out["Parse"].iloc[i] = "Step 2"

	# ---Step 3: Review all others---
	else:
		city_out["Accept"].iloc[i] = 0

# split dataframe by acceptance flag

print("Creating output files...")

house_accepted_g = house_out[house_out["Accept"] == 1]
house_accepted_b = house_out[house_out["Accept"] == 2]
house_review = house_out[(house_out["Accept"] == 0) | (pd.isnull(house_out["Accept"]))]

intersection_accepted_g = intersection_out[intersection_out["Accept"] == 1]
intersection_accepted_b = intersection_out[intersection_out["Accept"] == 2]
intersection_review = intersection_out[(intersection_out["Accept"] == 0) | (pd.isnull(intersection_out["Accept"]))]

street_accepted_g = street_out[street_out["Accept"] == 1]
street_accepted_b = street_out[street_out["Accept"] == 2]
street_review = street_out[(street_out["Accept"] == 0) | (pd.isnull(street_out["Accept"]))]

zip_accepted_g = zip_out[zip_out["Accept"] == 1]
zip_accepted_b = zip_out[zip_out["Accept"] == 2]
zip_review = zip_out[(zip_out["Accept"] == 0) | (pd.isnull(zip_out["Accept"]))]

city_accepted_g = city_out[city_out["Accept"] == 1]
city_accepted_b = city_out[city_out["Accept"] == 2]
city_review = city_out[(city_out["Accept"] == 0) | (pd.isnull(city_out["Accept"]))]

# merge correct address information into output fields
accepted_files = {
	0 : [house_accepted_g, house_accepted_b],
	1 : [intersection_accepted_g, intersection_accepted_b],
	2 : [street_accepted_g, street_accepted_b],
	3 : [zip_accepted_g, zip_accepted_b],
	4 : [city_accepted_g, city_accepted_b]
}

for i in range(len(accepted_files)):
	accepted_files[i][0]["U2_Address"] = nan
	accepted_files[i][0]["U2_City"] = nan
	accepted_files[i][0]["U2_State"] = nan
	accepted_files[i][0]["U2_ZIP"] = nan
	accepted_files[i][0]["U2_Y"] = nan
	accepted_files[i][0]["U2_X"] = nan
	accepted_files[i][0]["U2_ACCU"] = nan
	accepted_files[i][0]["U2_SOURCE"] = nan
	accepted_files[i][0]["U2_STATUS"] = nan

	accepted_files[i][0]["U2_Address"] = accepted_files[i][0]["Google Address"]
	accepted_files[i][0]["U2_City"] = accepted_files[i][0]["Google City"]
	accepted_files[i][0]["U2_State"] = accepted_files[i][0]["Google State"]
	accepted_files[i][0]["U2_ZIP"] = accepted_files[i][0]["Google ZIP"]
	accepted_files[i][0]["U2_Y"] = accepted_files[i][0]["Google Latitude"]
	accepted_files[i][0]["U2_X"] = accepted_files[i][0]["Google Longitude"]
	accepted_files[i][0]["U2_ACCU"] = accepted_files[i][0]["Google Accuracy"]
	accepted_files[i][0]["U2_SOURCE"] = 1
	accepted_files[i][0]["U2_STATUS"] = 3

	accepted_files[i][1]["U2_Address"] = nan
	accepted_files[i][1]["U2_City"] = nan
	accepted_files[i][1]["U2_State"] = nan
	accepted_files[i][1]["U2_ZIP"] = nan
	accepted_files[i][1]["U2_Y"] = nan
	accepted_files[i][1]["U2_X"] = nan
	accepted_files[i][1]["U2_ACCU"] = nan
	accepted_files[i][1]["U2_SOURCE"] = nan
	accepted_files[i][1]["U2_STATUS"] = nan

	accepted_files[i][1]["U2_Address"] = accepted_files[i][1]["Bing Address"]
	accepted_files[i][1]["U2_City"] = accepted_files[i][1]["Bing City"]
	accepted_files[i][1]["U2_State"] = accepted_files[i][1]["Bing State"]
	accepted_files[i][1]["U2_ZIP"] = accepted_files[i][1]["Bing ZIP"]
	accepted_files[i][1]["U2_Y"] = accepted_files[i][1]["Bing Latitude"]
	accepted_files[i][1]["U2_X"] = accepted_files[i][1]["Bing Longitude"]
	accepted_files[i][1]["U2_ACCU"] = accepted_files[i][1]["Bing Accuracy"]
	accepted_files[i][1]["U2_SOURCE"] = 2
	accepted_files[i][1]["U2_STATUS"] = 3

# merge accepted Google and accepted Bing dataframes, drop unneeded variables
# NOTE, ORIGINAL ADDRESS COLUMNS WILL BE DROPPED FROM LIST AFTER PROCESS IS REVIEWED
keep_cols = [
	"RecordNo",
	"Q",
	"Address",
	"City",
	"State",
	"ZIP",
	"Parse",
	"U2_Address",
	"U2_City",
	"U2_State",
	"U2_ZIP",
	"U2_Y",
	"U2_X",
	"U2_ACCU",
	"U2_SOURCE",
	"U2_STATUS"
]

house_accepted = pd.concat([house_accepted_g, house_accepted_b], ignore_index = True)
# house_accepted = house_accepted[keep_cols]

intersection_accepted = pd.concat([intersection_accepted_g, intersection_accepted_b], ignore_index = True)
# intersection_accepted = intersection_accepted[keep_cols]

street_accepted = pd.concat([street_accepted_g, street_accepted_b], ignore_index = True)
# street_accepted = street_accepted[keep_cols]

zip_accepted = pd.concat([zip_accepted_g, zip_accepted_b], ignore_index = True)
# zip_accepted = zip_accepted[keep_cols]

city_accepted = pd.concat([city_accepted_g, city_accepted_b], ignore_index = True)
# city_accepted = city_accepted[keep_cols]

# write outputs to excel for checking
output_files = {
	0 : ["House Address", house_accepted, house_review],
	1 : ["Intersection", intersection_accepted, intersection_review],
	2 : ["Street", street_accepted, street_review],
	3 : ["ZIP", zip_accepted, zip_review],
	4 : ["City", city_accepted, city_review],
	5 : ["Global Review", pd.DataFrame(), global_review]
}

for i in range(len(output_files)):
	if output_files[i][1].shape[0] == 0:
		output_files[i][1].to_csv(output_files[i][0] + " accepted (no records).csv", index = False)
	else:
		output_files[i][1].to_csv(output_files[i][0] + " accepted.csv", index = False)

	if output_files[i][2].shape[0] == 0:
		output_files[i][2].to_csv(output_files[i][0] + " review (no records).csv", index = False)
	else:
		output_files[i][2].to_csv(output_files[i][0] + " review.csv", index = False)

merge_out.to_csv("Unparsed Records.csv", index = False)

print("Output files created. Script complete.")